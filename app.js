$(function() {
  var loginDialog, loginForm;
  var registrationDialog, registrationForm;
  
  loginDialog = $("#login-dialog").dialog({
    autoOpen: false,
    modal: true
  });
  
  
  $("#login-link").click(function(event) {
    event.preventDefault();
    loginDialog.dialog("open");
  });
  
  registrationDialog = $("#registration-dialog").dialog({
    autoOpen: false,
    modal: true
  });
  
  $("#registration-button").button().on("click", function() {
    registrationDialog.dialog("open");
  });
  
  try{
    $("#protocols").DataTable();
  }
  catch(e) {
  }
  
  $("#certifications").dynatable();
  $("#accounts").dynatable();
  $("#history").DataTable();
  $("#users").dynatable();
  $('#forms').dynatable();
  $("#new").DataTable();
  
  $(".vote-link").click(function() {
    $.confirm({
      useBootstrap: false,
      closeIcon: true,
      alignMiddle: true,
      title: 'Vote on Protocol',
      content: 'Click YES or NO to put in your official vote on the current protocol.',
      backgroundDismiss: true,
      buttons: {
        yes: {
          btnClass: 'btn-green'
        },
        no: {
          btnClass: 'btn-red'
        }
      }
    });
  });
  

    $("#reset").click(function() {
      $("#forgotDialog").dialog();
    });

    // LOGIN FUNCTION
    $("#login").submit(function(e)
        {
            var postData = $(this).serializeArray();
            var formURL = $(this).attr("action");
            $.ajax(
            {
                url : formURL,
                type: "POST",
                data : postData,
                success:function(data, textStatus, jqXHR) 
                {
                    if(data == "successful")
                        window.location.reload(true);
                    
                    else if(data == "inactive") {
                        $.alert({
                            title: 'Inactive Account',
                            content: 'The account you logged in is not currently activated.',
                            useBootstrap: false,
                        });                    
                        
                        }
                    else {
                        $.alert({
                            title: 'Incorrect Login',
                            content: 'The data you provided did not match an account on record. Please try again.',
                            useBootstrap: false,
                        });
                    }
                        
                },
                error: function(jqXHR, textStatus, errorThrown) 
                {
                    alert("Bad login");
                }
            });
            e.preventDefault(); //STOP default action
            e.unbind(); //unbind. to stop multiple form submit.
        });
    
    // PASSWORD FORGOT ACTION
    $("#forgotit").submit(function(e) {
            var postData = $(this).serializeArray();
            var formURL = $(this).attr("action");
    });
      
    // REGISTRATION FORM ACTION
    $("#register").submit(function(e) {
        var postData = $(this).serializeArray();
        var formURL = $(this).attr("action");
        $.ajax({
          url : formURL,
          type: "POST",
          data : postData,
          success:function(data, textStatus, jqXHR)
            {
              if(data == "successful") {
                  $("#register").text("Thanks for signing up! Close this window, then click 'Login' on the top right.");
              }
              
              else if (data == "email") {
                  $.alert({
                            title: 'Invalid Email',
                            content: 'Please enter a valid e-mail address.',
                            useBootstrap: false,
                        }); 
              }
              
              else if (data == "password") {
                  $.alert({
                            title: 'Password Too Short',
                            content: 'Please enter a password that is at least 8 characters.',
                            useBootstrap: false,
                        }); 
              }
            },
          error: function(jqXHR, textStatus, errorThrown) 
            {
              $("#register").text("Sorry, something went wrong with your signup.");
            }
        });
        e.preventDefault(); //STOP default action
      });
      
    // CONTACT FORM ACTION
    $("#contact").submit(function(e)
    {
        var postData = $(this).serializeArray();
        var formURL = $(this).attr("action");
        $.ajax(
        {
            url : formURL,
            type: "POST",
            data : postData,
            success:function(data, textStatus, jqXHR) 
            {
                $("#contact").html("<br><hr><p>Your report is important and has been sent. If you entered a valid email address, your concern will be addressed via email</p>" +
                "<h3>Email you submitted: " + $("#email").val() + "</h3>" +
                "<h3>Content:</h3><p>\"" + $("#submitme").val() + "\"</p>" +
                "<hr><p><i>We sincerely appreciate you taking time to let us know about your concerns!</i></p>");
            },
            error: function(jqXHR, textStatus, errorThrown) 
            {
                $("#contact").text("Sorry, something went wrong. Please try again.");
            }
        });
        e.preventDefault();
        $('#submit').attr('disabled','disabled');
    });  

});



