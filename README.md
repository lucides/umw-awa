# UMW Animal Welfare Assurance

## Team Members
Soobin Choi, Kevin Hartnett, Hannah Morgan

## Overview
The Animal Welfare Assurance website is a web application system used to submit and track animal use for research and experimentation at UMW. All educational activities involving live, vertebrate animals by students, faculty, or staff must be reviewed and approved by the Institutional Animal Use and Care Committee (IACUC). The system will consist of a database and a web user interface to manage all animal protocols and related elements.
