<div class="container">
  <h2 id="iacuc">Institutional Animal Care and Use Committee</h2>
  <small>The oversight committee for animal use at UMW</small>
  
  <p>Any teaching or research activities involving live vertebrate animals (excluding humans) 
  by a member of the campus community (faculty, students, staff) must, by federal  regulations, 
  be reviewed and approved by a group known as the Institutional Animal Care and Use Committee (IACUC).  
  The University’s IACUC  has a document which details the policies and procedures that will apply to 
  live vertebrate animal use at UMW.</p>

  <?php
    if(!isset($_SESSION["usercat"]) || $_SESSION["usercat"] < 2)
      echo '<button class="btn-center" id="registration-button">Request Account</button>';
  ?>
  
  <div id="registration-dialog" class = "registration-dialog" title="Register">
    <form action="views/protected/process/register.php" method="post" id = "register">
    <div class = "fpad"><label>Full Name</label> 
    <span class = "required">*</span><input id="name" placeholder = "Jane Doe" name="name" type="text" class = "umwText" required></div>
    <div class = "fpad"><label>UMW Email</label>
    <span class = "required"> *</span><input id="email" name="email" placeholder = "jhu827@umw.edu" type="text" class = "umwText" required></div>
    <div class = "fpad"><label>Password</label><span class = "required">*</span><input id = "password" name = "password" placeholder = "password" type = "password" class = "umwText" required></div>
    <div class="fpad"><label>I am a...</label><span class="required">*</span>
    <select name = "type">
        <option value="2">Faculty Member</option>
        <option value="3">IACUC Member</option>
        <option value="4">IACUC Veterinarian</option>
    </select>
    </div>
    <center><input id="submit" type="submit" class = "umwButton" value="Register"></center>
    </form>
  </div>
  
</div>
