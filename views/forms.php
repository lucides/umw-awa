<div class = "container">
    <h2>Animal Protocol Form Repository</h2>
    <p><strong>Please fill all appropriate sections and appendices in the Full Animal Use 
    Protocol Application for upload and submission to the IACUC.</strong></p>
    
    <p>You must have an account to submit an animal use protocol for review.</p>
    
    <p>All separate appendices and forms are also in the table below for reference.</p>

    <table id = "forms">
        <thead>
            <tr>
                <th>Form</th>
            </tr>
        </thead>
        <tbody>
          <tr><td><a href="/download/UMW-IACUC-Animal-Protocol-Application-All.pdf">** FULL Animal Use Protocol Application **</a></td></tr>
          <tr><td><a href="/download/UMW-IACUC-Animal-Use-Protocol-Application.pdf">Animal Use Protocol Application</a></td></tr>
          <tr><td><a href="/download/UMW-IACUC-AppendixA.pdf">Animal Use Protocol Appendix A</a></td></tr>
          <tr><td><a href="/download/UMW-IACUC-AppendixE.pdf">Animal Use Protocol Appendix E</a></td></tr>
          <tr><td><a href="/download/UMW-IACUC-AppendixS.pdf">Animal Use Protocol Appendix S</a></td></tr>
          <tr><td><a href="/download/UMW-IACUC-AppendixT.pdf">Animal Use Protocol Appendix T</a></td></tr>
          <tr><td><a href="/download/UMW-IACUC-AppendixU.pdf">Animal Use Protocol Appendix U</a></td></tr>
          <tr><td><a href="/download/UMW-IACUC-AppendixW.pdf">Animal Use Protocol Appendix W</a></td></tr>
          <tr><td><a href="/download/UMW-IACUC-Exempt-Study-Field-Form.pdf">Animal Use Protocol Exempt Study Field Form</a></td></tr>
        </tbody>
    </table>
</div>