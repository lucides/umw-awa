<div class="container">
  <?php
    session_start();
    
    $link = pg_connect("host=ec2-54-204-0-88.compute-1.amazonaws.com port=5432 dbname=d4h25bnqedsrtb user=czbvmjibjyksje password=4bb5e504aa11e7d9ee5ea80cbe42eb7d717a033f1169eb22441809c4598c4264");
    
    if ($_SESSION["usercat"] > 1 && $_SESSION["usercat"] < 5) {
      echo '
        <h2>Voting History</h2>
        
        <table id="history">
          <thead>
            <tr style="color:white;">
              <th>Protocol</th><th>Submitted</th><th>Description</th><th>Files</th><th>Vote</th>
            </tr>
          </thead>
          <tbody>';
          
      $query = "SELECT * FROM votes,protocols WHERE votes.uid='" . $_SESSION['uid'] . "' AND votes.pid=protocols.pid ORDER BY vid DESC";
      $result = pg_query($query);
      if (!$result) { 
        echo "Problem with query " . $query . "<br/>"; 
        echo pg_last_error(); 
        exit(); 
      }
      
      while($myrow = pg_fetch_assoc($result)){
        if($myrow['vote']==2){$vote='Yes';}
        elseif($myrow['vote']==1){$vote='No';}
        printf("<tr class='tabl'> <td>%s</td> <td>%s</td> <td>%s...  <small><i><a href='#'>More</a></i></small></td> <td><a href='views/protected/uploads/%s.pdf'>Protocol%s.pdf</a><td>%s</td></tr>
          <tr class='hidden' hidden>  <td colspan=5>Full Description: %s</td></tr>
          <tr class='hidden' hidden>  <td colspan=5>Vote Comment: %s</td></tr>
          ",$myrow['title'],substr($myrow['submitdate'],0,10),substr($myrow['descrip'],0,30),$myrow['pid'],$myrow['pid'],$vote,$myrow['descrip'],$myrow['comment']
        );
      }

        echo '</tbody></table>';
    }
    
    else {
      echo '<h2>Oops!</h2>
            <p>You don\'t have access to this page.</p>';
    }
  ?>
</div>

<script>

  //this part by Kevin
  var counter = 0;
  $(".tabl").click(function() {
      
      if(counter % 2 === 0) {
        $(this).closest(".plusminus").text("[-]");
        $(this).closest('tr').css("background-color","grey")
        $(this).closest('tr').next().show("fast");
        $(this).closest('tr').next().next().show("fast");
        counter++;
      }
      else if(counter % 2 !== 0) {
        $(this).closest(".plusminus").text("[+]");
        $(this).closest('tr').css("background-color","white")
        $(this).closest('tr').next().hide("fast");
        $(this).closest('tr').next().next().hide("fast");
        counter++;
      }
  });
  //end part by Kevin

</script>