<div class="container">
  
<script>
$(document).ready(function() {
    $('#submit-protocol').on('click', function() {
        var theDialog = $("#dialog").dialog({ width: 'auto' });
        theDialog.dialog("open");
    });
});
</script>

  <?php
  
    session_start();
    
    $link = pg_connect("host=ec2-54-204-0-88.compute-1.amazonaws.com port=5432 dbname=d4h25bnqedsrtb user=czbvmjibjyksje password=4bb5e504aa11e7d9ee5ea80cbe42eb7d717a033f1169eb22441809c4598c4264");
    
    if ($_SESSION["usercat"] == 2) {
      echo '
        <h2>My Protocols</h2>
        <button class="btn-primary" id="submit-protocol"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;Submit Protocol</button>
        <table id="protocols">
          <thead>
            <tr style="color:white;">
              <th>Protocol</th><th>Submitted</th><th>Description</th><th>Files</th><th>Status</th>
            </tr>
          </thead>
          <tbody>';
          
      $currentuid=$_SESSION['uid'];
      $query = "SELECT * FROM protocols WHERE uid=" . $_SESSION['uid'] .  "ORDER BY submitdate DESC";
      $result = pg_query($query);
      if (!$result) { 
        echo "Problem with query " . $query . "<br/>"; 
        echo pg_last_error(); 
        exit(); 
      }
      
      while($myrow = pg_fetch_assoc($result)){
        if($myrow['status']==0){$stat='Pending';}
        elseif($myrow['status']==1){$stat='Disapproved';}
        else{$stat='Approved';}

        
        
          printf("<tr class='tabl'> <td>%s</td> <td>%s</td> <td>%s...  <small><i><a href='#'>More</a></i></small></td> <td><a target = '_blank' href='views/protected/uploads/%s.pdf'>Protocol%s.pdf</a></td> <td>%s</td> </tr>
              <tr class='hidden' hidden>  <td colspan='5'><span class='desc'>Full Description:</span> %s</td></tr>"
              ,$myrow['title'],substr($myrow['submitdate'],0,10),substr($myrow['descrip'],0,30),$myrow['pid'],$myrow['pid'],$stat,$myrow['descrip']
          );
        
      }

      echo '</tbody></table>';
      
    }
    
    else if ($_SESSION["usercat"] == 3 || $_SESSION['usercat'] == 4) {
      
      //voting options start
      
          echo '
            <script>
              $(document).on("click", ".yes", function () {
                  var com = prompt("Enter in any comments:");
                  var vote = 2;
                  $.get( "views/protected/vote.php", { comment: com, pid: $(this).attr("data-id"), vote: vote});
                  document.getElementById($(this).attr("data-id")).innerHTML = "VOTED YES!";
              });
              
              $(document).on("click", ".no", function () {
                  var vote = 1;
                  var com1 = prompt("Enter in your reason for voting against the protocol:");
                  $.get( "views/protected/vote.php", { comment: com1, pid: $(this).attr("data-id"), vote:vote});
                  document.getElementById($(this).attr("data-id")).innerHTML = "VOTED NO!";
              });
              </script>';
             
              //voting options end
      echo '
        <h2>Pending Protocols</h2>
        
        <table id="protocols">
          <thead>
            <tr style="color:white;">
              <th>Protocol</th><th>Submitted</th><th>Description</th><th>Files</th><th>Vote</th>
            </tr>
          </thead>
          <tbody>';
          
      $query = "SELECT * FROM protocols WHERE status=0"; // INACCURATE; don't go by status
      $result = pg_query($query);
      if (!$result) { 
        echo "Problem with query " . $query . "<br/>"; 
        echo pg_last_error(); 
        exit(); 
      }
              

        
      while($myrow = pg_fetch_assoc($result)){
        
        $vQuery = "SELECT comment FROM votes WHERE pid = " . $myrow['pid'] . " AND uid = " . $_SESSION['uid'] . ";";
        $vQueryResult = pg_query($vQuery);
        $fResult = pg_fetch_row($vQueryResult)[0];
        
        if(!$fResult) 
          printf("<tr class='tabl'> <td>%s</td> <td>%s</td> <td>%s...  <small><i><a href='#'>More</a></i></small></td> <td><a href='views/protected/uploads/%s.pdf'>Protocol%s.pdf</a></td> <td><div id = '" . $myrow['pid'] . "'><a data-id = '" . $myrow['pid'] . "' class = 'yes' href='#'>APPROVE</a> | <a href='#' data-id = '" . $myrow['pid'] . "' class = 'no'>DENY</a></div></td></tr>
            <tr class='hidden' hidden>  <td colspan=5><span class='desc'>Full Description:</span> %s</td></tr>
            ",$myrow['title'],substr($myrow['submitdate'],0,10),substr($myrow['descrip'],0,30),$myrow['pid'],$myrow['pid'],$myrow['descrip']
          );
      }

        echo '</tbody></table>';
      
    }
    
    else if ($_SESSION["usercat"] == 5) {
      echo '
        <h2>All Protocols</h2>
        <small>This table contains all protocols that have been submitted in the past three years.</small>
        <h2></h2>
        <table id="protocols_a">
          <thead>
            <tr>
              <th class = "white">ID</th><th class = "white">Protocol</th><th class = "white">Submitted</th><th class = "white">Description</th><th class = "white">Files</th><th class = "white">Status</th><th></th>
            </tr>
          </thead>';
          
        $query = "SELECT * FROM protocols ORDER BY submitdate DESC";
        $result = pg_query($query); 
        if (!$result) { 
          echo "Problem with query " . $query . "<br/>"; 
          echo pg_last_error(); 
          exit(); 
        } 
          
        while($myrow = pg_fetch_assoc($result)){

          if($myrow['status']==0){$stat='Pending';}
          elseif($myrow['status']==1){$stat='Disapproved';}
          else{$stat='Approved';}       
          
          printf("<tr class = 'tabl2'>  <td>%s</td> <td>%s</td> <td>%s</td> <td>%s</td> <td><a href='views/protected/uploads/%s.pdf'>Protocol%s.pdf</a></td> 
          <td>%s</td> <td><a href='#' \" class=\"data2\" data-id=".$myrow['pid'].">DELETE</a></td></tr>"
          , '<span class = "plusminus"><b>[+]</b></span>',$myrow['title'],substr($myrow['submitdate'],0,10),$myrow['descrip'],$myrow['pid'],$myrow['pid'],$stat);
          
          $query2 = "SELECT * FROM votes WHERE votes.pid='{$myrow['pid']}' ";
          $result2 = pg_query($query2);
          if(!$result2){
            echo "Problem with query " . $query2 . "<br/>"; 
            echo pg_last_error(); 
            exit(); 
          }
          
          for($i=0; $i<5; $i++){
            if($myrow2 = pg_fetch_assoc($result2)){
              printf("<tr class = 'hidden' hidden>  
                <td colspan='2'>IACUC Member ID %s</td>  
                <td><p><span class='desc'>Vote:</span> %s</p></td>  
                <td colspan='4'><p><span class='desc'>Comment:</span> %s</p></td><td></td><td></td><td></td><td></td></tr>", 
                $myrow2['uid'], $myrow2['vote'], $myrow2['comment']);
            }
            else{
              printf("<tr class = 'hidden' hidden>  
                <td colspan='2'>IACUC Member ID --</td>  
                <td><p><span class='desc'>Vote:</span> --</p></td>  
                <td colspan='4'><p><span class='desc'>Comment:</span> --</p></td><td></td><td></td><td></td><td></td></tr>");
            }
          }
          // $result3 = pg_query($query2);
          // $iterate = 0;
          // while($myrow3 = pg_fetch_assoc($result3)){
          //   printf("<tr class = 'hidden' hidden>  
          //   <td colspan='2'>IACUC Member ID %s</td>  
          //   <td><p><span class='desc'>Vote:</span> %s</p></td>  
          //   <td colspan='4'><p><span class='desc'>Comment:</span> %s</p></td><td></td><td></td><td></td><td></td></tr>", 
          //   $myrow3['uid'], $myrow3['vote'], $myrow3['comment']);
          //   $iterate++;
          // }
          // for($i==$iterate; $i<5; $i++){
          //   printf("<tr class = 'hidden' hidden>  
          //   <td colspan='2'>IACUC Member ID --</td>  
          //   <td><p><span class='desc'>Vote:</span> --</p></td>  
          //   <td colspan='4'><p><span class='desc'>Comment:</span> --</p></td><td></td><td></td><td></td><td></td></tr>");
          // }
        }

        echo '</tbody></table>';
        
        echo '
          <script>
          $(document).on("click", ".data2", function() {
            $.get("views/protected/deleteprotocol.php", {pid: $(this).data("id")});
            $.alert({
              title: "Deleted Protocol",
              content: "You deleted the protocol.",
              useBootstrap: false,
            });
            $(this).parents("tr").hide();
          });
          </script>';
    }
    
    else {
      echo '<h2>Oops!</h2>
            <p>You don\'t have access to this page.</p>';
    }
  ?>
</div>

<script>

  //this part by Kevin
  var counter = 0;
  $(".tabl").click(function() {
      
      if(counter % 2 === 0) {
        $(this).closest(".plusminus").text("[-]");
        $(this).closest('tr').css("background-color","grey")
        $(this).closest('tr').next().show("fast");
        counter++;
      }
      else if(counter % 2 !== 0) {
        $(this).closest(".plusminus").text("[+]");
        $(this).closest('tr').css("background-color","white")
        $(this).closest('tr').next().hide("fast");
        counter++;
      }
  });

  
  var counter = 0;
  $(".tabl2").click(function() {
    
      if(counter % 2 === 0) {
        $(this).closest(".plusminus").text("[-]");
        $(this).closest('tr').css("background-color","grey")
        $(this).closest('tr').next().show("fast");
        $(this).closest('tr').next().next().show("fast");
        $(this).closest('tr').next().next().next().show("fast");
        $(this).closest('tr').next().next().next().next().show("fast");
        $(this).closest('tr').next().next().next().next().next().show("fast");
        counter++;
      }
      else if(counter % 2 !== 0) {
        $(this).closest(".plusminus").text("[+]");
        $(this).closest('tr').css("background-color","white")
        $(this).closest('tr').next().hide("fast");
        $(this).closest('tr').next().next().hide("fast");
        $(this).closest('tr').next().next().next().hide("fast");
        $(this).closest('tr').next().next().next().next().hide("fast");
        $(this).closest('tr').next().next().next().next().next().hide("fast");
        counter++;
      }
  });
  //end part by Kevin


</script>



<div id="dialog" title="Upload Your Protocol Here" hidden>
  <form id = "upload-form" action = "views/protected/process/upload-protocol.php" enctype="multipart/form-data" method = "POST">
    <label><b>Protocol Name:</b> </label><input type = "text" name = "name" id = "name"><br><br>
    <label><b>Select PDF file:</b> </label>
    <input type = "file" name = "protocol-pdf" id = "protocol-pdf"><br><br>
    <label><b>Comments</b> (up to 1024 characters):</label><br><textarea name = "comments" id = "comments"></textarea><br><br>
    <input type = "submit" id = "submit1" name = "submit" value = "Upload Protocol" class = "umwText">
  </form>
</div>

