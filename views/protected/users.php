<!-- This page allows super-admin to see users -->
<!-- http://www.designmagick.com/article/10/Starting-Out/Retrieving-Data-from-your-database -->
<div class="container">
  <?php
    session_start();
    
    $link = pg_connect("host=ec2-54-204-0-88.compute-1.amazonaws.com port=5432 dbname=d4h25bnqedsrtb user=czbvmjibjyksje password=4bb5e504aa11e7d9ee5ea80cbe42eb7d717a033f1169eb22441809c4598c4264");
    
    if ($_SESSION["usercat"] == 5) {
      echo '
        <h2>Users</h2>
        <small>This table contains all current user accounts and any accounts pending activation.</small>
        <small>Here is the categories of users are. Please make sure a user requested the right category.</small>
        <ol>
        <li>Guest [No Privileges]</li>
        <li>Faculty Member [Can Submit Protocols]</li>
        <li>Committee Member [Can Vote on Protocols]</li>
        <li>Veterinarian on Committee [Can Vote/Veto Protocols]</li>
        <li>Superadministrator [Should only be you!]</li>
        </ol>
        <h2></h2>
        
        <table id="users">
          <thead>
            <tr>
              <th>Name</th><th>Category</th><th>Email</th><th>Actions</th>
            </tr>
          </thead>
          <body>';
          
          $query = "SELECT * FROM users ORDER BY activated";
          $result = pg_query($query);
          if (!$result) { 
            echo "Problem with query " . $query . "<br/>"; 
            echo pg_last_error(); 
            exit(); 
          } 
          
          while($myrow = pg_fetch_assoc($result)){
            if($myrow['activated']==0){
              printf("<tr id = \"" . $myrow['uid'] . "\"><td>%s</td><td>%s</td><td>%s</td><td><a href ='#'" . $myrow['uid'] . "\" class = \"data1\" data-id = " . $myrow['uid'] . ">APPROVE</a> | <a href = '#' \" class = \"data3\" data-id = " . $myrow['uid'] . ">REMOVE</a></td></tr>", $myrow['name'],$myrow['category'],$myrow['email']);
            }
            elseif ($myrow['category']>1) {
              printf("<tr id = \"" . $myrow['uid'] . "\"><td>%s</td><td>%s</td><td>%s</td><td><a href = '#' \" class = \"data2\" data-id = " . $myrow['uid'] . ">DELETE</a> </td></tr>", $myrow['name'],$myrow['category'],$myrow['email']);
            }
          }
          
          echo '</tbody></table>';
          
          echo '
            <script>
              $(document).on("click", ".data1", function () {
                  $.get( "views/protected/approve.php", { uid: $(this).data("id")} );
                  $.alert({
                    title: "Approved Account",
                    content: "You approved the account!",
                    useBootstrap: false,
                  });
                  $(this).parents("tr").hide();
              });
              
              $(document).on("click", ".data2", function () {
                  $.get( "views/protected/delete.php", { uid: $(this).data("id")} );
                  $.alert({
                    title: "Deleted Account",
                    content: "You deleted the account!",
                    useBootstrap: false,
                  });  
                  $(this).parents("tr").hide();
              });
              $(document).on("click", ".data3", function () {
                  $.get( "views/protected/delete.php", { uid: $(this).data("id")} );
                  $.alert({
                    title: "Account Request Removed",
                    content: "You have denied this account request.",
                    useBootstrap: false,
                  });
                  $(this).parents("tr").hide();

              });

              </script>';
    }
    
    else {
      echo '<h2>Oops!</h2>
            <p>You don\'t have access to this page.</p>';
    }
  ?>
</div>
