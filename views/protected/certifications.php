<div class="container">
  
  <script>
    $(document).ready(function(){
      $('#submit-certif').on('click', function(){
        var theDialog = $("#dialog").dialog({ width: 'auto'});
        theDialog.dialog("open");
      });
    });
  </script>
  
  <?php
    session_start();
    
    $link = pg_connect("host=ec2-54-204-0-88.compute-1.amazonaws.com port=5432 dbname=d4h25bnqedsrtb user=czbvmjibjyksje password=4bb5e504aa11e7d9ee5ea80cbe42eb7d717a033f1169eb22441809c4598c4264");
    
    if ($_SESSION["usercat"] > 1) {
      echo '
        <h2>Training Certifications Database</h2>';
      if($SESSION["usercat"]==2){
        echo '
          <small>This page contains detailed information about students and faculty that are certified to work with animals.
          You may also upload a certification you have completed, or a certification on behalf of a student.</small>';
      } else{
        echo '
          <small>This page contains detailed information about students and faculty that are certified to work with animals.
          You may upload a certification on behalf of an individual in the UMW research community.</small>';
      }
      echo '
        <button class="btn-primary" id="submit-certif"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;Submit Certification</button>
        <table id="certifications">
          <thead>
            <tr>
              <th>Name</th><th>Position</th><th>Cert Number</th><th>Certification</th><th>Date Completed</th>
            </tr>
          </thead>
          <tbody>';
        

      $query = "SELECT * FROM certifs ORDER BY personname ASC";
      $result = pg_query($query);
      if (!$result) { 
        echo "Problem with query " . $query . "<br/>"; 
        echo pg_last_error(); 
        exit(); 
      }
      
      while($myrow = pg_fetch_assoc($result)){
        if($myrow['usercat']==0){$posit='Student';}
        elseif($myrow['usercat']==2){$posit='Faculty';}
        printf("<tr>
          <td>%s</td> <td>%s</td> <td>%s</td> <td>%s</td> <td>%s</td> </tr>
          ", $myrow['personname'],$myrow['position'],$myrow['certifnum'],$myrow['certiftitle'],$myrow['certifdate'],$myrow['cid']);
      }

      echo '</tbody></table>';

    }
    
    // else if ($_SESSION["usercat"] == 5) {
    //   echo '
    //     <h2>Training Certifications Database</h2>
    //     <small>This page contains detailed information about students and faculty that are certified to work with animals.</small>
    //     <button class="btn-primary" id="submit-certif"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;Submit Certification</button>
    //     <table id="certifications">
    //       <thead>
    //         <tr>
    //           <th>Name</th><th>Position</th><th>Cert Number</th><th>Certification</th><th>Date Completed</th><th>File</th><th></th>
    //         </tr>
    //       </thead>';

    //   $query = "SELECT * FROM certifs ORDER BY personname ASC";
    //   $result = pg_query($query);
    //   if (!$result) { 
    //     echo "Problem with query " . $query . "<br/>"; 
    //     echo pg_last_error(); 
    //     exit(); 
    //   }
      
    //   while($myrow = pg_fetch_assoc($result)){
    //     if($myrow['usercat']==0){$posit='Student';}
    //     elseif($myrow['usercat']==2){$posit='Faculty';}
    //     printf("<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td><a href='#'>Certif%sFile</a></td><td><a href='#' \" class=\"data2\" data-id=".$myrow['cid'].">DELETE</a></td></tr>", $myrow['personname'],$posit,$myrow['certifnum'],$myrow['certiftitle'],$myrow['certifdate'],$myrow['cid']);
    //   }

    //   echo '</tbody></table>';
      
    //   echo '
    //     <script>
    //     $(document).on("click", ".data2", function() {
    //       $.get("views/protected/deletecertif.php", {cid: $(this).data("id")});
    //       $.alert({
    //         title: "Deleted Certification",
    //         content: "You deleted the certification.",
    //         useBootstrap: false,
    //       });
    //       $(this).parents("tr").hide();
    //     });
    //     </script>';

    // }
    
    else {
      echo '<h2>Oops!</h2>
            <p>You don\'t have access to this page.</p>';
    }
  ?>
</div>

<div id="dialog" title="Upload a Certification Here" hidden>
  <h3>Please populate all fields unless marked optional.</h3>
  <form id = "upload-form" action = "views/protected/process/upload-certif.php" enctype="multipart/form-data" method = "POST">
    <label><b>Name:</b> </label><input type = "text" name = "name" id = "name"><br><br>
    <label><b>Position:</b> </label><input type = "radio" name="posit" value="Faculty"> Faculty   <input type="radio" name="posit" value="Student"> Student<br><br>
    <label><b>Certification Number:</b></label><input type="text" name="num" id="num"><br><br>
    <label><b>Certification Title:</b></label><input type="text" name="cert" id="cert"><br><br>
    <label><b>Date Completed (YYYY-MM-DD):</b></label><input type="text" name="date" id="date"><br><br>
    <input type = "submit" id = "submit1" name = "submit" value = "Submit" class = "umwText">
  </form>
</div>