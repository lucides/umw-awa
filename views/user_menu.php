<?php
  
  session_start();
  
  // if user is logged in
  if ($_SESSION['usercat'] > 1) {
    echo '
      <script>
        $("#registration-button").hide();
        $("#login-link").text("Logout");
      </script>
    ';
  }
  
  if ($_SESSION['usercat'] == 2) {
    echo '
      <nav class="container" id="sub-nav">
        <div class="nav-list">
          <ul class="menu-left">
            <li><a href="?page=protocols"><i class="fa fa-archive" aria-hidden="true"></i>&nbsp;&nbsp;My Protocols</a></li>
            <li><a href="?page=certifications"><i class="fa fa-certificate" aria-hidden="true"></i>&nbsp;&nbsp;Certifications Database</a></li>
          </ul>
        </div>
      </nav>
    ';
  }
  
  else if ($_SESSION['usercat'] == 3 || $_SESSION['usercat'] == 4) {
    echo '
          <nav class="container" id="sub-nav">
        <div class="nav-list">
          <ul class="menu-left">
            <li><a href="?page=protocols"><i class="fa fa-archive" aria-hidden="true"></i>&nbsp;&nbsp;Pending Protocols</a></li>
            <li><a href="?page=certifications"><i class="fa fa-certificate" aria-hidden="true"></i>&nbsp;&nbsp;Certifications Database</a></li>
            <li><a href="?page=vote_history"><i class="fa fa-history" aria-hidden="true"></i>&nbsp;&nbsp;Voting History</a></li>
          </ul>
        </div>
      </nav>
    ';
  }
  
  else if ($_SESSION['usercat'] == 5) {
    echo '
          <nav class="container" id="sub-nav">
        <div class="nav-list">
          <ul class="menu-left">
            <li><a href="?page=protocols"><i class="fa fa-archive" aria-hidden="true"></i>&nbsp;&nbsp;Protocols</a></li>
            <li><a href="?page=certifications"><i class="fa fa-certificate" aria-hidden="true"></i>&nbsp;&nbsp;Certifications Database</a></li>
            <li id = "usersMenu"><a href="?page=users"><i class="fa fa-users" aria-hidden="true"></i>&nbsp;&nbsp;Users</a></li>
          </ul>
        </div>
      </nav>
    ';
  }


?>