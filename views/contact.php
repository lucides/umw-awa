<div class = "container">
    <h2>Contact Info</h2>
    <p>If you would like to fill out an <i>anonymous</i> report on suspected
    animal cruelty or improper procedures stemming from a study approved
    by our office, please fill out the form below. Your complaint will be 
    addressed promptly.</p>
    <form id = "contact" name = "contact" type = "POST" action = "views/protected/process/send_contact.php">
    <h2>Fill Out Anonymous Report</h2>
    <label>Email: </label><input type = "email" class = "umwText" name = "email" id = "email">
    <br><br>
    <textarea id = "submitme" class = "umwText" name = "content"></textarea>
    <br>
    <br>
    <div class="g-recaptcha" data-sitekey="6LfjJRoUAAAAANSl9r2rMHiaYa5oAnVxIsVG3JpJ"></div>
    <br>
    
    <input type = "submit" class = "umwButton" value = "Send Report" id = "submit">
    </form>
</div>
