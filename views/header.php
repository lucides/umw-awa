<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>UMW Animal Welfare Assurance</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="app.js"></script>
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.1.0/jquery-confirm.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.1.0/jquery-confirm.min.js"></script>
  <script src = "js/jquery.dynatable.js"></script>
  <link rel = "stylesheet" href = "js/jquery.dynatable.css">
  <link rel="stylesheet" href="main.css">
  <script src = "js/jquery.fileuploads.js"></script>
  <script src = "js/jquery.dataTables.js"></script>
  <link href = "js/jquery.dataTables.css" rel = "stylesheet">
</head>
<body>

<!--<img src = "img/logo.png" max-height = "200" max-width = "200" style = "display: inline; padding-right: 50px">-->

<nav id="main-nav">
  <div class="nav-list">
    <ul class="menu-left">
      <li><a href="?page=home">Home</a></li>
      <li><a href="?page=forms">Forms</a></li>
      <li><a href="?page=contact">Contact</a></li>
    </ul>
    <ul class="menu-right">
     <?php session_start();
            if(!isset($_SESSION["email"]))
                echo '
                    <li><a href="#" id="login-link">Login</a></li>';
            else
                echo '
                    <li><a href = "views/protected/process/logout.php">Logout</a></li>';
    ?>
    </ul>
    <?php session_start(); if(isset($_SESSION["email"])) { echo '<span class = "loggedIn">Logged in: ' . $_SESSION["email"] . '</span>'; 
        echo '&nbsp;<span class = "loggedIn">Status: ' . $_SESSION["category"] . '</span>'; }?>
  </div>
</nav>

<div id="login-dialog" title = "Login">
  <form action="views/protected/process/login.php" method="post" id = "login">
    <div class = "fpad"><label>Email</label> 
    <span class = "required">*</span><input id="email" name="email" placeholder = "jhu827@umw.edu" type="text" class = "umwText" required></div>
    <div class = "fpad"><label>Password</label>
    <span class = "required">*</span><input id = "password" name = "password" type = "password" class = "umwText" placeholder = "••••••••" required></div>
    <center><input id="submit" type="submit" class = "umwButton" value="Login"></center>
  </form>
  
  <p id = "reset"><b><a href = "#">Forgot Password? Click here.</a></b></p>
  
</div>

<div id = "forgotDialog" title = "Forgot Password!" hidden>
  <form id = "forgotit" action = "" method = "POST">
    <div class = "fpad"><label>Email</label> 
    <span class = "required">*</span><input id="email" name="email" placeholder = "jhu827@umw.edu" type="text" class = "umwText" required></div>
  </form>
    <input id="submit" type="submit" class = "umwButton" value="Login">
</div>