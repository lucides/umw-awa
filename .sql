-- DROP TABLE IF EXISTS users CASCADE;
-- DROP SEQUENCE users_uid_seq IF EXISTS uid_seq;

-- CREATE SEQUENCE users_uid_seq;

-- CREATE TABLE users(
--   uid int NOT NULL DEFAULT nextval('users_uid_seq') PRIMARY KEY,
--   name text NOT NULL,
--   category int NOT NULL, -- 2-faculty,3-member,4-vet,5-super
--   email text NOT NULL,
--   password text NOT NULL,
--   activated int NOT NULL DEFAULT 0
-- );

--superadmin,pending faculty,faculty,committee member,vet
-- INSERT INTO users (name,category,email,password,activated) VALUES ('Deborah O''Dell',5,'dodell@umw.edu','default',1); --1
-- INSERT INTO users (name,category,email,password,activated) VALUES ('Jane Doe',2,'jdoe@umw.edu', 'dummy',0);      --2
-- INSERT INTO users (name,category,email,password,activated) VALUES ('Andrew Ash',2,'aash@umw.edu','dummy',1);    --3
-- INSERT INTO users (name,category,email,password,activated) VALUES ('Zac Zins',2,'zzins@umw.edu','dummy',1);     --4
-- INSERT INTO users (name,category,email,password,activated) VALUES ('Bernice Ball',3,'bball@umw.edu','dummy',1); --5
-- INSERT INTO users (name,category,email,password,activated) VALUES ('Cecil Cyan',4,'ccyan@umw.edu','dummy',1);   --6

DROP TABLE IF EXISTS protocols CASCADE;
DROP SEQUENCE IF EXISTS protocols_id_seq;

CREATE SEQUENCE protocols_id_seq;

CREATE TABLE protocols(
  pid int NOT NULL DEFAULT nextval('protocols_id_seq') PRIMARY KEY,
  uid int, --gained from the session
  name text NOT NULL,
  numanimals int, --bogus column now
  title text NOT NULL,
  submitdate timestamp NOT NULL DEFAULT current_timestamp,
  descrip text,
  status int NOT NULL DEFAULT 0, -- 0 is pending, 1 is disapproved, 2 is approved?
  FOREIGN KEY (uid) REFERENCES users (uid)
);

INSERT INTO protocols (uid,name,title,descrip) VALUES (3,'Andrew Ash','Zebrafish Experiment','Studying fin development. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.');
INSERT INTO protocols (uid,name,title,descrip) VALUES (3,'Andrew Ash','Newt Experiment','Studying social behavior. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.');
INSERT INTO protocols (uid,name,title,descrip) VALUES (4,'Zac Zins','Mice Experiment','Behavioral experiment. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.');



DROP TABLE IF EXISTS certifs CASCADE;
DROP SEQUENCE IF EXISTS certifs_id_seq;

CREATE SEQUENCE certifs_id_seq;

CREATE TABLE certifs(
  cid int NOT NULL DEFAULT nextval('certifs_id_seq') PRIMARY KEY,
  personName text NOT NULL,
  position text NOT NULL,
  certifTitle text NOT NULL,
  certifNum int NOT NULL,
  certifDate date NOT NULL
);

INSERT INTO certifs (personName,position,certifTitle,certifNum,certifDate) VALUES ('Anna Williams','Student','Mice Certificate',345882,'1/1/2017');
INSERT INTO certifs (personName,position,certifTitle,certifNum,certifDate) VALUES ('Adrian Washington','Student','Mice Certificate',345882,'4/1/2017');
INSERT INTO certifs (personName,position,certifTitle,certifNum,certifDate) VALUES ('Sam Estes','Faculty','Lizard Certificate',604820,'3/30/2017');



DROP TABLE IF EXISTS votes CASCADE;
DROP SEQUENCE IF EXISTS votes_id_seq;

CREATE SEQUENCE votes_id_seq;

CREATE TABLE votes(
  vid int NOT NULL DEFAULT nextval ('votes_id_seq') PRIMARY KEY,
  pid int NOT NULL,
  uid int NOT NULL,
  vote int NOT NULL, --2 is YES, 1 is NO
  comment text,
  FOREIGN KEY (uid) REFERENCES users (uid),
  FOREIGN KEY (pid) REFERENCES protocols (pid)
);

INSERT INTO votes (pid,uid,vote,comment) VALUES (1,5,1,'Unclear procedure, Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis.');
INSERT INTO votes (pid,uid,vote,comment) VALUES (2,5,2,'Acceptable, if this change is made: suscipit laboriosam, nisi ut aliquid.');
INSERT INTO votes (pid,uid,vote,comment) VALUES (1,6,2,'Ethically appropriate. Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur.');
INSERT INTO votes (pid,uid,vote,comment) VALUES (3,6,1,'Cannot approve as is. Vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?');


