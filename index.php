<!-- All page template includes go here -->
<?php
  session_start();
  
  //Set a user category for testing
  //usercat 2 is a FACULTY member
  //usercat 3 is a COMMITTEE member
  //usercat 4 is the VET
  //usercat 5 is the SUPERADMIN
  
  //activated 0 is deactivated
  //activated 1 is activated account
  
 // USE THE SYNTAX ?usercat=1 to view as guest, ?usercat=2 to view as faculty, etc
  
  ////////DELETE AFTER TESTING
  if(isset($_GET['usercat'])) {
    $_SESSION['usercat'] = $_GET['usercat'];
  }
  
  if(isset($_GET['uid'])){
      $_SESSION['uid'] = $_GET['uid'];
  }
  ////////END DELETE AFTER TESTING

  include("views/header.php");
  include("views/user_menu.php");

  if(!isset($_GET['page'])) {
    include ("views/home.php");
  }
  
  else if ($_GET['page'] == 'home') {
    include("views/home.php");
  }
  
  else if($_GET['page'] == 'protocols') {
    include("views/protected/protocols.php");
  }
  
  else if($_GET['page'] == 'certifications') {
    include("views/protected/certifications.php");
  }
  
  else if($_GET['page'] == 'vote_history') {
    include("views/protected/vote_history.php");
  }
  
  else if($_GET['page'] == 'vote') {
    include("views/protected/vote.php");
  }
  else if($_GET['page'] == 'users') {
    include("views/protected/users.php");
  }
  
  else if ($_GET['page'] == 'forms') {
    include("views/forms.php");
  }

  else if ($_GET['page'] == 'contact') {
    include("views/contact.php");
  }
  
  else if ($_GET['page']=='new'){
      include("views/new.php");
  }

  else {
    include("views/404.php");
  }

  include("views/footer.php");

 ?>
